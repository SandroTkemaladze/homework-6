package com.example.homework6

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE)
    }


    fun save(view:View){
        val email = EmailET.text.toString()
        val firstName = firstNameET.text.toString()
        val lastName = lastNameET.text.toString()
        val age = AgeET.text.toString().toInt()
        val address = AddressET.text.toString()

        val edit = sharedPreferences.edit()
        edit.putString("email",email)
        edit.putString("firstName",firstName)
        edit.putString("lastName",lastName)
        edit.putInt("age",age)
        edit.putString("Address",address)
        edit.apply()
    }
    
}